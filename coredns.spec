%define project github.com/coredns/coredns
%global debug_package %{nil}
Name:           coredns
Version:        1.7.0
Release:        1.1
Summary:        CoreDNS is a DNS server/forwarder, written in Go
License:        Apache-2.0
URL:            https://coredns.io
Source0:        https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz
Source1:        vendor.tar.gz

BuildRequires:  golang >= 1.13
BuildRequires:  fdupes

%description
CoreDNS is a fast and flexible DNS server. The key word here is flexible:
with CoreDNS you are able to do what you want with your DNS data by
utilizing plugins.

%package help
Summary:        Help document for the coredns package

%description help
Help document for the coredns package.

%prep
%setup -q -a1

%build
GO_BUILD_PATH=$PWD/_build
install -m 0755 -vd $(dirname $GO_BUILD_PATH/src/%{project})
ln -fs $PWD $GO_BUILD_PATH/src/%{project}
cd $GO_BUILD_PATH/src/%{project}
export GOPATH=$GO_BUILD_PATH:%{gopath}
export PATH=$PATH:$GOPATH/bin
go build -mod=vendor -v -buildmode=pie -o coredns

# It is not recommended to test in the build environment,
# because some testcases will be affected by the factors of
# the build environment and fail.
# It is recommended to execute the use cases manually

%install
# Binaries
strip coredns
install -D -m 0755 coredns %{buildroot}%{_sbindir}/coredns

# Manpages
install -d %{buildroot}/%{_mandir}/man1
install -m 0644 man/coredns*.1 %{buildroot}/%{_mandir}/man1
install -d %{buildroot}/%{_mandir}/man5
install -m 0644 man/corefile*.5 %{buildroot}/%{_mandir}/man5
install -d %{buildroot}/%{_mandir}/man7
install -m 0644 man/coredns-*.7 %{buildroot}/%{_mandir}/man7

%fdupes %{buildroot}/%{_prefix}

%files
%{_sbindir}/coredns
%license LICENSE

%files help
%{_mandir}/man1/coredns*
%{_mandir}/man5/corefile*
%{_mandir}/man7/coredns-*

%changelog
* Wed Sep 08 2021 WuJing <wujing50@huawei.com> 1.7.0-1.1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: strip coredns

* Thu Mar 11 2021 WuJing <wujing50@huawei.com> - 1.7.0-1.0
- Initial Packaging
